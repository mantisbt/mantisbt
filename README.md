# Mantis Binary Tree

This repository contains a simple and efficient implementation of a binary tree in PHP.

A binary tree is a hierarchical data structure in which each node has at most two children,
often referred to as the left and right child. Binary trees are useful for various applications,
such as searching, sorting, and organizing data.

## Features

- Easy-to-use interface for creating and manipulating binary trees
- Basic tree traversal algorithms
- Insertion and search operations
- Node removal
- Counting the number of nodes in the tree

## Installation

To install the Mantis Binary Tree Library, simply grab it from packagist.org using composer:

```
composer required mantisbt/mantisbt
```

## Usage

### Creating a binary tree

To create a new binary tree, instantiate the 'Tree' class:

```
use MantisBT\Tree;
$tree = new Tree();
```

### Adding values to the tree

To insert values into the truee, use the `add` method:

```
$tree->insert(42);
$tree->insert(10);
$tree->insert(60);
```

### Searching for a node in the tree

To search for a value in the tree, use the `find` method:

```
$node = $tree->find(61);

if ($node !== null ) {
  echo "Value Found: " . $node->value . PHP_EOL;
} else {
  echo "Value Not Found " . PHP_EOL;
}
```

### Traversal

The tree can be manually traversed by using:

- getLeft()
- getRight()
- getParent()

## Contributing

If you would like to contribute to this project, please feel free to submit a pull request or create an issue to discuss your ideas, or contact us via our website at https://mantisbt.dev

## License

This PHP Binary Tree library is released under the MIT License.

## Changelog

0.1 - Initial Release
0.2 - Add SetValue command to Node to allow modifications to tree node values
0.3 - Fix bug in Find function