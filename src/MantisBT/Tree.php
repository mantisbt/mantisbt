<?php

/** Mantis Binary Tree **/
namespace MantisBT;

/**
 * Represents a binary tree that can contain nodes.
 *
 */
class Tree
{
	/**
	 * Root Node
	 * @private Node|null
	 */
	private $root;

	/**
	 * 
	 * @var int
	 */
	private $maxLevel = 0;

	/**
	 * @return int
	 */
	public function getMaxLevel()
	{
		return $this->maxLevel;
	}

	/**
	 * @param int $maxLevel
	 * @return Tree
	 */
	public function setMaxLevel($maxLevel)
	{
		$this->maxLevel = $maxLevel;
		return $this;
	}

	/**
	 * Add a new node, using the insert function to put it into the correct location.
	 *
	 * @param   mixed       $value  New node being added.
	 * @return  Tree          Returns the tree.
	 */
	public function add($value)
	{
		$node = new Node($value);

		if ($this->root === null) {
			$node->setLevel(1);
			$this->root = $node;
		} else {
			$this->insert($node, $this->root);
		}

		return $this;
	}

	/**
	 * Insert a new node into the tree.
	 *
	 * @param Node      $node     Node object to be inserted into the tree.
	 * @param Node|null $subtree  Next node in the tree.
	 * @param int             $level    Current level of the tree.
	 */
	private function insert(Node $node, &$subtree, $level = 0)
	{
		if ($this->getMaxLevel() < ++$level) {
			$this->maxLevel = $level;
		}

		if (!$subtree instanceof Node) {
			$node->setLevel($level);
			$subtree = $node;
		} else {
			$node->setParent($subtree);
			if ($node->value > $subtree->value) {
				$this->insert($node, $subtree->right, $level);
			} else if ($node->value < $subtree->value) {
				$this->insert($node, $subtree->left, $level);
			}
		}
	}

	/**
	 * Find the parent node of two given values.
	 *
	 * @param   mixed       $value1     Value of a child.
	 * @param   mixed       $value2     Value of another child.
	 * @return  Node  Parent node for the two values.
	 */
	public function parentFinder($value1, $value2)
	{
		// Find value 1
		$node1 = $this->find($value1);

		// Find value 2
		$node2 = $this->find($value2);

		while ($node1->getParent() !== $node2->getParent()) {
			if ($node1->getLevel() > $node2->getLevel()) {
				$node1 = $node1->getParent();
			} else if ($node2->getLevel() > $node1->getLevel()) {
				$node2 = $node2->getParent();
			} else if ($node1->getLevel() === $node2->getLevel()) {
				$node1 = $node1->getParent();
				$node2 = $node2->getParent();
			}
		}

		return $node1->getParent();
	}

	/**
	 * Find a node for a given value.
	 *
	 * @param mixed         $value  Value of a node.
	 * @return Node           Node object for the given value if one is found.
	 */
	public function find($value)
	{
		return $this->getRoot()->find($value);
	}

	/**
	 * Get the root node of the binary tree.
	 *
	 * @return Node|null  Root node of the binary tree.
	 */
	public function getRoot()
	{
		return $this->root;
	}

}