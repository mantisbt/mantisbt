<?php

/** Mantis Binary Tree **/
namespace MantisBT;

/**
 * Represents a single node in a binary tree.
 */
class Node
{
	/**
	 * Parent Node
	 * @private Node
	 */
	private $parent;

	/**
	 * Level
	 * @public int
	 */
	public $level;

	/**
	 * value of the node
	 * @public mixed
	 */
	public $value;

	/**
	 * Child node to the left
	 * @public Node
	 */
	public $left;

	/**
	 * Child node to the right
	 * @public Node
	 */
	public $right;

	/**
	 * Node constructor.
	 *
	 * @param mixed $value Value for the node.
	 */
	public function __construct($value = null)
	{
		$this->value = $value;
	}

	/**
	 * @return Node
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param Node $parent
	 * @return Node
	 */
	public function setParent(Node $parent)
	{
		$this->parent = $parent;
		return $this;
	}

	/**
	 * Get value of this node
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param Node $value
	 * @return Node
	 */
	public function setValue(Node $value)
	{
		$this->value = $value;
		return $this;
	}	

	/**
	 * Return child node to the left
	 * @return Node|null
	 */
	public function getLeft()
	{
		return $this->left;
	}

	/**
	 * Return child node to the right
	 * @return Node|null
	 */
	public function getRight()
	{
		return $this->right;
	}

	/**
	 * Get current level in tree
	 * @return int
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 * @param int $level
	 * @return Node
	 */
	public function setLevel($level)
	{
		$this->level = $level;
		return $this;
	}

	/**
	 * Find the child node with the given value.
	 *
	 * @param mixed $value Value to find
	 * @return Node|null Node
	 */
	public function find($value)
	{
		if ($value === $this->getValue()) {
			return $this;
		} else if ($value < $this->getValue()) {
			$left = $this->getLeft();
			if( $left !== null )
				return $left->find($value);
		} else if ($value > $this->getValue()) {
			$right = $this->getRight();
			if( $right !== null ) {
				return ($this->getRight()?->find($value));
			}
		}

		return null;
	}
}