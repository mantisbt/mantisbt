<?php

use MantisBT\Tree;

/**
 * Test cases
 */
class BTTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Add Nodes
     */
    public function testAdd()
    {
        $bt = new Tree();
        $bt->add(4)->add(5)->add(3)->add(4)->add(1);

        $this->assertEquals(4, $bt->getRoot()->getValue());
        $this->assertEquals(3, $bt->getRoot()->getLeft()->getValue());
        $this->assertEquals(3, $bt->getMaxLevel());
        $this->assertEquals(1, $bt->getRoot()->getLevel());
        $this->assertEquals(2, $bt->getRoot()->getRight()->getLevel());
    }

    /**
     * Verify that the parent lookup finds the correct parent.
     */
    public function testParentFinder()
    {
        $bt = new Tree();
        $bt
            ->add(7)
			->add(5)
            ->add(8)
            ->add(11);

        $this->assertEquals(7, $bt->parentFinder(8,11)->getValue());
    }
}